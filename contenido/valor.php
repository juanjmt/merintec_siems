<table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <td>
        <img class="imagen_izq" src="imagenes/imgs-cont/valor.jpg" width="924" height="211">
        <p class="titulosverdes">Valor agregado</p>
        <p class="titulosverdes">¿Qué Ofrecemos?</p>
       <span class="texto">
        <blockquote>
          <p>▪ Atención personalizada<br />
            ▪ Aulas equipadas con tecnología de punta<br />
            ▪ Instructores expertos en las áreas específicas (con Especialización, Maestría o Doctorado) y con amplia experiencia en el ámbito empresarial<br />
            ▪ Programas de formación actualizados y cuidadosamente diseñados<br />
            ▪ Material de Apoyo cuidadosamente dise-ñado para contribuir a alcanzar los objetivos de aprendizaje<br />
            ▪	Certificado de asistencia o aprobación<br />
            ▪	Garantía de un servicio de alto nivel<br />
          </p>
        </blockquote>
        <p class="titulosverdes"> ¿Por qué elegirnos?<p>
         
          1. Hemos capacitado con éxito a más de 25.000 personas, somos reconocidos por nuestra calidad y excelencia por instituciones prestigiosas de la Ciudad de Mérida y por importantes empresas de la región y del país.</p>
        <p>2. Contamos con las acreditaciones	de:<br />
          ▪ El Ministerio del Poder Popular para la Educación para efectuar capacitación.<br />
          ▪ El INCES para Efectuar Adiestramiento Deducible a empresas.<br />
          ▪ El INCES como Instituto Educativo de Acción Docente Delegada para la formación de aprendices.</p>
        <p>3. Contamos con un equipo multidisciplinario de profesionales expertos, con gran trayectoria y alto nivel de formación académica y experiencia laboral, con estudios de cuarto nivel, especialización, maestría o doctorado.</p>
        <p>4. Nuestro staff está compuesto por personas que aman su trabajo y que han entendido que los clientes y su satisfacción es nuestra principal razón de existencia.</p>
        <p>5. Contamos con instalaciones cómodas y confortables dotadas de avanzada tecnología al servicio de una educación de alto nivel.</p>
        <p>6. Nuestro servicio se complementa con material de apoyo cuidadosamente diseñado para contribuir a alcanzar los objetivos de aprendizaje.</p>
        <p>7. Contamos con el Área de Desarrollo Profesional para beneficio exclusivo de nuestros clientes tanto estudiantes como empresas. Este programa tiene como misión contribuir con la gestión de la carrera profesional de nuestros egresados y alumnos y aportar esta valiosa información a las organizaciones que la requieran.</p>
        <p>8. Ofrecemos programas actualizados, hechos a la medida de nuestros clientes. </p></span>
        </td>
  </tr>
</table>
