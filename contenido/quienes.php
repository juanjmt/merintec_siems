<table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <td>
        <!--<img class="imagen_izq" src="imagenes/imgs-cont/quienes.jpg" width="300" height="255">-->
        <p class="titulosverdes">¿Quienes Somos?</p>
        <span class="texto"><p>SIEMS es un Instituto de Estudios Gerenciales de  orientación global con aplicación local, que ofrece  programas de cuarto nivel  (maestrías) y de educación continua (diplomados, cursos,  seminarios y congresos) de alto nivel,  apoyados en el mejor talento humano, avanzada tecnología y metodologías innovadoras de aprendizaje, con el fin de formar líderes, profesionales, gerentes y empresarios  capaces de responder exitosamente a los desafíos de su entorno de negocio.<br />
          <br />
          Esta Institución se fundó  en 1999 y desde su creación se ha caracterizado por  su excelencia académica, la constante mejora y adaptación de sus programas de formación a las necesidades cambiantes del contexto venezolano y por contar con facilitadores de alto nivel académico y experiencia profesional.<br />
          <br />
          Uno de los valores agregados de la formación que ofrece Siems, son las distintas opciones de certificación, logradas a través de sus convenios con la Asociación de Naciones Unidas de Venezuela –ANUV-, Universidad Pedagógica  Experimental Libertador y otras organizaciones nacionales e internacionales. <br />
          <br />
          Herramientas para tú futuro y herramientas para la excelencia y la productividad, han sido el eslogan que resume el aspecto diferenciador de esta institución, que ha procurado el aprendizaje significativo de tipo constructivista  fundamentado en la combinación de la experiencia de los estudiantes, la experiencia de los facilitadores con las teorías y conceptos globales aplicados al contexto local.<br />
          <br />
          Siems enfrenta nuevos retos en un entorno cambiante, pero cuenta con personal motivado, capacitado y comprometido con la excelencia para hacer de esta organización  el Instituto de Estudios Gerenciales de mayor reconocimiento y prestigio a nivel regional,  apalancados en  la innovación, personal docente de alto nivel, excelentes instalaciones físicas y metodologías innovadoras de aprendizaje, que propicien el éxito profesional de nuestros socios de aprendizaje. </p>
        </span>
        </td>
  </tr>
</table>
