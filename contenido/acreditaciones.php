<table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <td><p><img src="imagenes/imgs-cont/acredit.jpg" width="924" height="211" /></p>
      <p class="titulosverdes">Acreditaciones</p>
        <span class="texto"><p>Siems Cuenta con las siguientes Autorizaciones, Registros y Convenios: <br />
          ▪ Autorización del Ministerio de Educación. Ver modelo de certificado<br />
          ▪ Autorización de INCES para Efectuar Adiestramiento Deducible a empresas a través de nuestra fundación FUNDACEO.<br />
          •	Registro en el SIDCAI del Ministerio de Ciencia y Tecnología, como institución beneficiaria de aportes LOCTI para ciencia y Tecnología. Las actividades docentes que imparte SIEMS pueden ser reconocidas como inversión en ciencia, tecnología e innovación LOCTI (Art.42), si las mismas se encuentran dentro del plan de formación del personal de cada organización.<br />
          •	Convenios que nos permiten unas importantes opciones de certificación nacionales e internacionales: <br />
          UPEL y Convenios con: la Asociación de Naciones Unidas de Venezuela –ANUV-, Universidad Pedagógica  Experimental Libertador,  Universidad Nacional Experimental Del Táchira (Unet) –  Cátedra Libre Eleanor Roosevelt  y Otras Organizaciones Nacionales e Internacionales.  Instituto Universitario Europeo Di Studi Sociali Italia, Unión Europea, Instituto Internacional de Estudios Globales Para el Desarrollo Humano, International University For Global Studies,  Unesco Center Foundation For Human Rights Training,Global Citizenship And Culture Of Peace,  World Federation Of United Nations Associations (Wfuna) y la  Plataforma Internacional Para La Gestión Empresarial Ética. </p>
      <p align="center"><img src="imagenes/imgs-cont/logoscert.jpg" width="595" height="296" /></p>
        <p><strong><u>Ver modelo de Certificado</u></strong><br />
      Estas acreditaciones no sólo generan confianza en la Institución, sino además, facilitan el proceso de contratación de los servicios de capacitación, lo cual, permite dar la garantía de una Capacitación Profesional de Alto Nivel a organizaciones y a particulares.</p></span>
</td>
  </tr>
</table>
