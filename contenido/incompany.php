<table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <td>
        <img class="imagen_izq" src="imagenes/imgs-cont/incompany.jpg" width="300" height="255">
        <p class="titulosverdes">Programas de Capacitación In-Company</p>
        <span class="texto"><p>SIEMS ofrece una serie de programas de capacitación in company, que pueden ser adaptados a las necesidades de cada organización en cuanto a objetivos, contenidos, dinámicas y número de participantes. Estos programas están especialmente diseñados para satisfacer las necesidades de formación de las organizaciones del sector público y privado.</p>
        <p>La capacitación del personal es una de las actividades que desarrolla toda gerencia de recursos humanos, comprometida con el crecimiento profesional de su personal y con el mejoramiento del performance de las actividades asociadas a cada cargo, ya que, permite desarrollar un lenguaje común entre todos los miembros de la organización y a generar un espíritu de equipo.</p>
          Además, podemos dictar en la modalidad in company, cualquier programa de capacitación ofertado regularmente en nuestra institución.</p>
        </span>
        </td>
  </tr>
</table>
