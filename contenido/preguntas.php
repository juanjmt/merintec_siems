<table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">

  <tr>
    <td><p><!--<img src="imagenes/imgs-cont/preguntas2.jpg" width="924" height="211" />--><span class="titulosverdes">Preguntas Frecuentes</span>
      <table width="901" height="1339" border="0" cellpadding="0" cellspacing="7">
        <tr>
          <td width="39">&nbsp;</td>
          <td width="841"><span class="textoresaltado">¿Cuál es el valor de un curso en SIEMS?</span><br />
<span class="texto">El precio de nuestros cursos varía en función del número de horas de cada curso, como cliente registrado puedes acceder a la tabla completa de aranceles. Puedes acceder a este link para registrarte: ___________ </span></td>
        </tr>
        <tr>
          <td bgcolor="#F9F3E0">&nbsp;</td>
          <td bgcolor="#f9f3e0"><span class="textoresaltado">¿Cuál es el procedimiento para inscribirme?</span><br />
            <span class="texto">El procedimiento es muy simple. <br />
            1. Usted puede llamar por teléfono al (0274)2632530 /416.4143 y también, a través de nuestro portal para realizar su preinscripción.<br />
            2. Puede también realizar el pago personalmente en nuestras instalaciones o efectuar depósito o transferencia en las siguientes cuentas bancarias: </p>
            <blockquote> 1.- Banesco: <br />
              Cuenta Corriente No. 0134-0244-2924-4300-6551<br />
              Titular: SIEMS de Jorge Molina <br />
              Rif: J-12348522-6<br />
  <br />
              2.-  Banco Mercantil:<br />
              Cuenta Corriente No. 0105-0092-3110-9206-8465<br />
              Titular: Siems  Servicios Integrados y Estudios Empresariales<br />
              Rif. R-12348522-6<br />
              Una vez efectuado el pago, debes adjuntarlo a email y notificarlo junto con tus datos y el nombre del curso de tu interés. Confirmado el pago, nosotros le enviaremos la factura escaneada.
</blockquote></span></td>
        </tr>
        <tr>
          <td height="538">&nbsp;</td>
          <td><span class="textoresaltado">¿Qué tipo de certificación obtendré? , ¿Quién avala los cursos en SIEMS?</span> <br />
            <span class="texto">Al finalizar cada curso, SIEMS otorgara según sea el caso certificado de asistencia o de aprobación. <br />
            Cada tipo de programa cuenta con acreditaciones distintas. En general contamos con:<br />
            ▪ Autorización del Ministerio de Educación para efectuar capacitación.<br />
            ▪ Autorización de INCES para Efectuar Adiestramiento Deducible a empresas a través de nuestra fundación FUNDACEO.<br />
            •	Convenios que nos permiten unas importantes certificaciones nacionales e internacionales para algunos de nuestros programas:</p>
            <p align="center"><img src="imagenes/imgs-cont/logoscert.jpg" width="595" height="296" /></p>
          <p>Convenios con: la Asociación de Naciones Unidas de Venezuela –ANUV-, Universidad Pedagógica  Experimental Libertador,  Universidad Nacional Experimental Del Táchira (Unet) –  Cátedra Libre Eleanor Roosevelt  y Otras Organizaciones Nacionales e Internacionales.  Instituto Universitario Europeo Di Studi Sociali Italia, Unión Europea, Instituto Internacional de Estudios Globales Para el Desarrollo Humano, International University For Global Studies,  Unesco Center Foundation For Human Rights Training,Global Citizenship And Culture Of Peace,  World Federation Of United Nations Associations (Wfuna) y la  Plataforma Internacional Para La Gestión Empresarial Ética. </span></td>
        </tr>
        <tr>
          <td bgcolor="#f9f3e0">&nbsp;</td>
          <td bgcolor="#f9f3e0"><span class="textoresaltado">¿Qué documentos debo presentar para inscribirme?</span> <br />
<span class="texto">Cédula de identidad y en caso de ser estudiante, carnet vigente para obtener precio preferencial. En el caso de las maestrías debe consignar título profesional universitario de licenciatura, ingeniería, etc. Para  los diplomados, copia de Título Universitario (TSU o Profesional Universitario).</span></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><span class="textoresaltado">¿Soy Extranjero, puedo realizar un curso en SIEMS?</span><br />
<span class="texto">Efectivamente. Usted puede tomar cualquiera de nuestros cursos</span></td>
        </tr>
        <tr>
          <td bgcolor="#f9f3e0">&nbsp;</td>
          <td bgcolor="#f9f3e0"><span class="textoresaltado">¿Necesito ir personalmente a SIEMS para inscribirme?</span> <br />
<span class="texto">No hay necesidad de ir personalmente. Puedes preincribirte telefónicamente  a través de los teléfono 0274-1632530 /426.41.43 o  de nuestra página web y abonar la inversión  haciendo depósito o transferencia en nuestras cuentas. </span></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><span class="textoresaltado">¿Puedo pagar con tarjeta de crédito, tarjeta de débito o cheque?</span><br />
<span class="texto">Si, Siems cuenta con el servicio de punto de venta bancario.</span></td>
        </tr>
        <tr>
          <td bgcolor="#f9f3e0">&nbsp;</td>
          <td bgcolor="#f9f3e0"><span class="textoresaltado">¿Tengo que pagar IVA ? </span><br />
<span class="texto">No, nuestros cursos están exentos de IVA, ya que, por contar con registro del Ministerio de Educación somos contribuyentes formales de IVA.</span></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><ul>
            <li><span class="texto">Contamos con un equipo multidisciplinario de profesionales expertos, con gran trayectoria y alto nivel de formación académica y amplia experiencia laboral, con estudios de cuarto nivel, especialización, maestría o doctorado.</span></li>
            <li><span class="texto">Nuestro staff está compuesto por personas que aman su trabajo y que han entendido que los clientes y su satisfacción es nuestra principal razón de existencia.</span></li>
            <li><span class="texto">Contamos con instalaciones VIP de entrenamiento,  dotadas de avanzada tecnología al servicio de una educación de alto nivel.</span></li>
            <li><span class="texto">Nuestro servicio se complementa con material de apoyo cuidadosamente diseñado para contribuir a alcanzar los objetivos de aprendizaje.</span></li>
            <li><span class="texto">Contamos con el Área Colocación Laboral para beneficio exclusivo de nuestros clientes tanto estudiantes como empresas. Este programa tiene como misión contribuir con la gestión de la carrera profesional de nuestros egresados y aportar esta valiosa información a las organizaciones que la requieran.</span></li>
            <li><span class="texto">Ofrecemos programas actualizados, hechos a la medida de nuestros clientes. </span></li>
          </ul></td>
        </tr>
        </span>
      </table></td>
  </tr>
  </span>
</table>
