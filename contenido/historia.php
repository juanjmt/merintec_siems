<table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <td>
        <img class="imagen_izq" src="imagenes/imgs-cont/acredit.jpg" width="262" height="255">
        <p class="titulosverdes">Historia</p>
        <span class="texto"><p>FALTA es un instituto de estudios gerenciales de  orientación global con aplicación local, que ofrece  diplomados, cursos,  seminarios y congresos de alto nivel apoyados en el mejor talento humano, avanzada tecnología y metodologías innovadoras de aprendizaje, con el fin de formar líderes, profesionales, gerentes y empresarios  capaces de responder exitosamente a los desafíos de su entorno de negocio.</p>
        <p>Esta Institución se fundó  en 1999 y desde su creación se ha caracterizado por  su excelencia académica, la constante mejora y adaptación de sus programas de formación a las necesidades cambiantes del contexto venezolano y por contar con facilitadores de alto nivel académico y experiencia profesional.</p>
        <p>Herramientas para tú futuro y herramientas para la excelencia y la productividad, han sido el eslogan que resume el aspecto diferenciador de esta institución, que ha procurado el aprendizaje de tipo constructivista fundamentado en la combinación de la experiencia de los estudiantes, la experiencia de los facilitadores con las teorías y conceptos globales aplicados al contexto local.</p>
        <p>Siems enfrenta nuevos retos en un entorno cambiante, pero cuenta con personal motivado, capacitado y comprometido con la excelencia para hacer de esta organización  el Instituto de estudios gerenciales de mayor reconocimiento y prestigio a nivel regional,  apalancados en  la innovación, personal docente de alto nivel, excelentes instalaciones físicas y metodologías innovadoras de aprendizaje, que propicien el éxito profesional de nuestros clientes.</p></span>
        </td>
  </tr>
</table>
