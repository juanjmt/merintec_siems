<table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <td><p class="titulosverdes"><img src="imagenes/imgs-cont/outdoor.jpg" width="924" height="211" /></p>
      <p class="titulosverdes">Programas de Formación Outdoor</p>
        <span class="texto"><p>Hoy en día para el éxito de las organizaciones y la diferenciación competitiva es necesario gestionar adecuadamente el Capital Humano, es decir el talento, creatividad, inteligencia, aspiraciones y motivaciones del personal de una empresa, enmarcados dentro de una cultura organizacional que define su accionar, permitiendo responder con agilidad a los requerimientos cambiantes del mundo moderno. <br />
          Está comprobado científicamente que aprendemos el 20% de lo que escuchamos, el 50% de lo que vemos y el 80% de lo que hacemos.<br />
          Esto prueba que la experiencia vivencial de los empleados frente a cursos o conferencias tradicionales en las capacitaciones empresariales serían mucho más enriquecedoras y efectivas. Aquí es cuando el sistema de formación al aire libre cobra importancia. <br />
          El Entrenamiento al Aire Libre: Mucho más que un Juego<br />
          Un programa de entrenamiento al aire libre va más allá de ofrecer cursos y juegos al aire libre, ya que se busca que el empleado debe asumir un compromiso de mejora con su empresa. Las actividades diseñadas parten de una planificación tomada de la problemática y necesidades detectadas. Se busca que el grupo detecte los puntos fuertes y débiles derivados de los comportamientos del equipo y vincular estas actitudes a las conductas habituales en el trabajo. Finalmente, se elabora un plan de acción con los diferentes aspectos a mejorar aplicables a la empresa.<br />
          Al igual que todo programa de formación desarrollado por una compañía, sus objetivos deben están claramente definidos, evaluando el momento en el cual se aplica y siendo administrado y dirigido por técnicos especialmente entrenados para ello. En ello se basa el éxito.<br />
          <br />
          <span class="textoresaltado">¿Cómo se desarrolla el proceso a través de ésta metodología?</span></p>
       
        <p>El entrenamiento al aire libre es un medio que consiste en juegos o actividades al aire libre o espacios abiertos, con una metodología propia de la educación experiencial, es decir aprendizaje netamente vivencial, que presenta una secuencia lógica de actividades donde se extraen conclusiones que ayudan a mejorar el entorno personal y profesional. <br />
          Es llevada a cabo por personal (facilitadores) con un alto contenido docente, que basan el aprendizaje a través de la experiencia en un clima distendido, donde se llevan a cabo actividades que combinan la competitividad, el trabajo en equipo, el liderazgo y la comunicación, es decir aspectos relevantes para la gestión del capital social. <br />
          <br />
          <span class="textoresaltado">¿Qué beneficios brinda ésta metodología a  las empresas?</span><br />
          <br />
          Los beneficios más relevantes están relacionados con las habilidades interpersonales propias de la inteligencia emocional que deben estar presente en las empresas, como son los siguientes:<br />
          <br />
          ▪	Estimula la confianza y desarrolla/potencia el espíritu de trabajo en equipo, para lograr la cooperación y complementación entre los miembros.<br />
          ▪	Potencia el liderazgo y la capacidad de delegar responsabilidades.<br />
          ▪	Motiva a los participantes en los compromisos del trabajo diario.<br />
          ▪	Fomenta la comunicación e integración entre áreas y miembros de una organización.<br />
          ▪	Aumenta la resistencia al estrés.<br />
          ▪	Mejora la reacción de sagacidad en ingenio ante situaciones de presión y cambio.<br />
          ▪	Aumenta la confianza entre compañeros.<br />
          ▪	Entrena el enfoque de la atención y visión.<br />
          <br />
          La riqueza que se obtiene de este tipo de experiencias impacta de manera positiva en los participantes. Su gran aporte personal y grupal establece un diferencial con instancias tradicionales de capacitación orientadas a un estilo académico.</p>
        </span></td>
  </tr>
</table>
