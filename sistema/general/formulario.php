<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <title>.:: SIEMS Instituto Gerencial ::.</title>
        <script src="sistema/validacion/js/jquery-1.8.2.min.js" type="text/javascript"></script>
		  <script src="sistema/validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
		  <script src="sistema/validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
		  <link rel="stylesheet" href="sistema/validacion/css/validationEngine.jquery.css" type="text/css"/>
		  <link rel="stylesheet" href="sistema/validacion/css/template.css" type="text/css"/>
		  <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
        <!-- <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script> -->
        <link rel="stylesheet" href="sistema/comunes/calendario/jquery-ui.css">
        <script src="sistema/comunes/calendario/jquery-ui.min.js"></script>
 
        <!-- validacion en vivo -->
        <script >
          jQuery(document).ready(function(){
	    // binds form submission and fields to the validation engine
              jQuery("#form1").validationEngine('attach', {bindMethod:"live"});
          });
        </script>
        <!-- CALENDARIO-->
  <script>
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			$( ".datepicker" ).datepicker({
				firstDay: 1,
				closeText: 'Cerrar',
				nextText: 'Sig ->',
				currentText: 'Hoy',
				prevText: '<- Ant',
				monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
				monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
				dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
				dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié;', 'Juv', 'Vie', 'Sáb'],
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
				dateFormat: 'dd/mm/yy',			
				changeYear: true
			});
			jQuery("form1").validationEngine();
		});	
	</script>
    </head>
  <body>
    <?php 
      $formulario = $_GET['formulario'];
      if ($formulario){
        include('sistema/formularios/'.$formulario.'.php');
      }
      else{
        include('sistema/formularios/base.php');
      }
    ?>
  </body>
</html>
