<?php 
include("sistema/comunes/verificar_admin.php");
$boton=$_POST['boton'];
$cedu_usua=$_POST['cedu_usua'];
$nomb_usua=$_POST['nomb_usua'];
$apel_usua=$_POST['apel_usua'];
$corr_usua=$_POST['corr_usua'];
$logi_usua=$_POST['logi_usua'];
$pass_usua=md5($_POST['pass_usua']);
$codg_usua=$_POST['codg_usua'];
$tipo_usua=$_POST['tipo_usua'];
$stat_usua=$_POST['stat_usua'];
$parametro=$_POST['parametro'];

$con['cedu_usua']=$_POST['cedu_usua'];
$con['nomb_usua']=$_POST['nomb_usua'];
$con['apel_usua']=$_POST['apel_usua'];
$con['corr_usua']=$_POST['corr_usua'];
$con['logi_usua']=$_POST['logi_usua'];
$con['pass_usua']=$_POST['pass_usua'];
$con['codg_usua']=$_POST['codg_usua'];
$con['tipo_usua']=$_POST['tipo_usua'];	
$con['stat_usua']=$_POST['stat_usua'];		


/// valores para usar auditoría en caso de eliminar y modificar
$auditoria=$_POST['campo_auditoria'];
/// Preparando datos para guardar
$tabla = "usuarios";
$key_entabla = 'codg_usua';
$key_enpantalla = $codg_usua;
$datos[0] = prepara_datos ("cedu_usua",$_POST['cedu_usua'],'');
$datos[1] = prepara_datos ("nomb_usua",$_POST['nomb_usua'],'');
$datos[2] = prepara_datos ("apel_usua",$_POST['apel_usua'],'');
$datos[3] = prepara_datos ("corr_usua",$_POST['corr_usua'],'');
$datos[4] = prepara_datos ("logi_usua",$_POST['corr_usua'],'');
$datos[5] = prepara_datos ("tipo_usua",$_POST['tipo_usua'],'');
$datos[6] = prepara_datos ("stat_usua",$_POST['stat_usua'],'');
$k = 6;
$k = $k+1; 
if ($_POST['codg_empr']==''){ $_POST['codg_empr'] = 'NULL'; }
$datos[$k] = prepara_datos ("codg_empr",$_POST['codg_empr'],'');
if ($_POST['pass_usua']!='') { $k = $k+1; $datos[$k] = prepara_datos ("pass_usua",md5($_POST['pass_usua']),'');}

if ($boton=='Guardar'){
	$buscando = buscar($tabla,'cedu_usua',$_POST[cedu_usua]." AND codg_usua<>".$_POST['codg_usua'],'individual');
	$buscando_cor = buscar($tabla,'corr_usua',$_POST[corr_usua]." AND codg_usua<>".$_POST['codg_usua'],'individual');
	if($buscando[1]<1 && $buscando_cor[1]<1){
		$ejec_guardar = guardar($datos,$tabla);
		if ($ejec_guardar[0]!=''){
			$existente='si';
			$$key_entabla = $ejec_guardar[0];
			$con2 = buscar($tabla,$key_entabla,$ejec_guardar[0],'individual');
			$con=$con2[0];
			$auditoria=$con2[3];
			$mensaje_mostrar=$ejec_guardar[1];
		}
		$mensaje_mostrar=$ejec_guardar[1];
	}else{
		if($buscando_cor[1]==1){
			$mensaje_mostrar = 'Error: El email '.$_POST[corr_usua].' ya existe, intente nuevamente';
		}else{
			$mensaje_mostrar = 'Error: La cédula '.$_POST[cedu_usua].' ya existe, intente nuevamente';
		}		
		$boton = '';			
	}	
	
}
if ($boton=='Eliminar')
{
	$buscar = buscar($tabla,$key_entabla,$key_enpantalla,'individual');
	$tipo_usuairo=$buscar[0][tipo_usua];	
	if ($tipo_usuairo==2 OR $tipo_usuairo==3){
		$mensaje_mostrar='No se puede eliminar debido a que tiene un perfil de Usuario Regular o Empresa asociado';
		$boton="Eliminando";
	}else{
		$ejec_eliminar = eliminar($tabla,$key_entabla,$key_enpantalla,$auditoria);
		$mensaje_mostrar=$ejec_eliminar;
		$boton='';
		$auditoria='';
	}
}
if ($boton=='Actualizar')
{
	$buscando = buscar($tabla,'cedu_usua',$_POST[cedu_usua]."' AND codg_usua<>'".$_POST['codg_usua'],'individual');
	$buscando_cor = buscar($tabla,'corr_usua',$_POST[corr_usua]."' AND codg_usua<>'".$_POST['codg_usua'],'individual');
	if($buscando[1]<1 && $buscando_cor[1]<1){
		$ejec_actualizar = actualizar($datos,$tabla,$key_entabla,$key_enpantalla,$auditoria);
		$existente='si';        
		$mensaje_mostrar=$ejec_actualizar[1];
		$$key_entabla = $ejec_actualizar[0];
		$con2 = buscar($tabla,$key_entabla,$ejec_actualizar[0],'individual');
		$con=$con2[0];
		$auditoria=$con2[3];
	}else{
		if($buscando_cor[1]==1){
			$mensaje_mostrar = 'Error: El email '.$_POST[corr_usua].' ya existe, intente nuevamente';
		}else{
			$mensaje_mostrar = 'Error: La cédula '.$_POST[cedu_usua].' ya existe, intente nuevamente';
		}		
		$boton = 'Actualizando';			
	}	
}
if ($boton=='Buscar')
{
	$buscando = buscar($tabla,$_POST['criterio'],$parametro,'general');
	$con=$buscando[0];
	$nresultados=$buscando[1];
	$mensaje_mostrar=$buscando[2];
	$auditoria=$buscando[3];
	$$key_entabla = $con[$key_entabla];
	if ($$key_entabla!=NULL) 
	{
		$existente='si';
        }
	else 
	{
		$existente='no';
		$boton='';
	}
}
if ($boton=='Nuevo')
{
	$con = array();
	$existente='no';
	$boton='';
	$auditoria='';
}
if ($boton=='Modificar')
{
	$con = buscar($tabla,$key_entabla,$key_enpantalla,'individual');
	$con=$con[0];
	$mensaje_mostrar = "Cambie la información que requiera y presione Actualizar";
	$existente='no';
	if ($con['tipo_usua']==2 || $con['tipo_usua']==3){
		$bloqueando = "readonly";
	}
}
if ($boton=='Actualizando')
{
	$boton = 'Modificar';
}
if ($boton=='Eliminando')
{
	$existente='si';
	$con = buscar($tabla,$key_entabla,$key_enpantalla,'individual');
	$con=$con[0];
	$boton='Buscar';
}
if ($_POST['aux'])
{
	$boton=$_POST['aux'];
}

//CONSULTAS COMBOS

$consulta_tusuarios = mysql_query("SELECT * FROM usuarios_tipos order by nomb_tusu ");
if ($con[tipo_usua]!='')
{
	       $tipo_usua=$con[tipo_usua];
       	 $consulta_tusuarios1 = mysql_query("SELECT * FROM usuarios_tipos where codg_tusu='$tipo_usua' ");
       	 $contu=mysql_fetch_assoc($consulta_tusuarios1);
       	 $nomb_tusu=$contu[nomb_tusu];

}

$consulta_empresas = mysql_query("SELECT * FROM empresas order by nomb_empr ");
if ($con[codg_empr]!='')
{
	       $codg_empr=$con[codg_empr];
       	 $consulta_empresas1 = mysql_query("SELECT * FROM empresas where codg_empr='$codg_empr' ");
       	 $conev=mysql_fetch_assoc($consulta_empresas1);
       	 $nomb_empr=$conev[nomb_empr];
}

?>

<meta charset="utf-8" />
	<div class="titulo_formulario" align="center">REGISTRO DE USUARIOS</div>
	<?php include('sistema/general/mensaje.php'); ?>
	<?php if ($nresultados>1){ 
		// definimos los parametros a mostrar en el resultado múltiple
		$buscar_varios[0][0]="Cédula";
		$buscar_varios[0][1]="cedu_usua";
		$buscar_varios[1][0]="Nombres";
		$buscar_varios[1][1]="nomb_usua";
		$buscar_varios[2][0]="Apellidos";
		$buscar_varios[2][1]="apel_usua";
		$buscar_varios[3][0]="Correo";
		$buscar_varios[3][1]="corr_usua";
		$buscar_varios[3][3]="center";
		$buscar_varios[4][0]="Empresa";
		$buscar_varios[4][1]="codg_empr";
		$buscar_varios[4][2]=array("empresas","codg_empr","nomb_empr");
		$buscar_varios[4][3]="center";
	    $buscar_varios[5][0]="Status";
		$buscar_varios[5][1]="stat_usua";
		$buscar_varios[5][3]="center";
		include('sistema/general/busqueda_varios.php'); 
		echo '<br>'; 
	} 
	else {?>
	<form id="form1" onsubmit="return jQuery(this).validationEngine('validate');"  method="post" action="">
		<table cellpaddig="0" cellspacing="0" border="0" align="center">
      </br>
      </br>	
      <?php 
         /// No Borrar campo usado para auditoría    
	      echo "<input type='hidden' name='campo_auditoria' value='".$auditoria."'>";
         ////////////////////////////////////////
			if ($existente!='si')
         {
				echo '<input type="hidden" name="codg_usua" id="codg_usua" value="'.$con['codg_usua'].'">';	
      		echo '
				<tr>
					<td align="center">	   
						<input type="text" class="validate[required, custom[onlyLetterNumber], minSize[6],maxSize[20]] text-input, cajas_entrada" '.$bloqueando.' value="'.$con[cedu_usua].'" id="cedu_usua" name="cedu_usua" placeholder="Cédula de Identidad" />
					</td>
				</tr>
				<tr>
          		<td  align="center">
						<input type="text" class="validate[required, custom[onlyLetterSp], minSize[3],maxSize[30]] text-input, cajas_entrada"  '.$bloqueando.' value="'.$con[nomb_usua].'" id="nomb_usua" name="nomb_usua" placeholder="Nombres" />
         		</td>
				</tr>
				<tr>
					<td align="center">
						<input type="text" class="validate[required, custom[onlyLetterSp] , minSize[3],maxSize[30]] text-input,  cajas_entrada"  '.$bloqueando.' value="'.$con[apel_usua].'" id="apel_usua" name="apel_usua" placeholder="Apellidos" />
					</td>
				</tr>

				<tr>
					<td align="center">
						<input type="text" class="validate[required, custom[email] , minSize[3],maxSize[100]] text-input,  cajas_entrada"  '.$bloqueando.' value="'.$con[corr_usua].'" id="corr_usua" name="corr_usua" placeholder="Correo Electrónico" />
					</td>
				</tr>


				<tr>
					<td align="center">
	
						<select name="tipo_usua" id="tipo_usua"  class="validate[required], combo_form" onchange="submit();">';
					if ($con[tipo_usua]==NULL)
					{ 
						 echo ' <option value="" selected disabled style="display:none;">Seleccione el Tipo de Usuario</option>';
       				}
       				else 
       				{
      					
       					echo' <option  ';  echo'  value="'.$tipo_usua.'" >'.$nomb_tusu.'</option> ';
       				}
       				while($fila=mysql_fetch_array($consulta_tusuarios))
                  {
                      echo "<option "; if ($_POST['tipo_usua']==$fila[codg_tusu]) { echo 'selected'; if ($fila[nomb_tusu]=='Empresa' || $fila[nomb_tusu]=='Empresas') { $mostrar_empresas ="si"; } } echo " value=".$fila[codg_tusu].">".$fila[nomb_tusu]."</option>";
                  }
						
						echo '</select>
						
					</td>
				</tr>';
				if ($mostrar_empresas == "si") { 				
				echo '<tr>
					<td align="center">
						<select name="codg_empr" id="codg_empr"  class="validate[required], combo_form" >';
						echo ' <option value="">Seleccione empresa</option>';
	       				if ($con[codg_empr]!=NULL)
      	 				{	
       						echo' <option selected value="'.$codg_empr.'" >'.$nomb_empr.'</option> ';
       					}
       					while($fila=mysql_fetch_array($consulta_empresas))
                  			{
                      				echo "<option value=".$fila[codg_empr].">".$fila[nomb_empr]."</option>";
	                  		}
						echo '</select>
					</td>
				</tr>';
				}
				echo '<tr>
					<td align="center">
						<select name="stat_usua" id="stat_usua"  class="validate[required], combo_form" >';
						if ($con[stat_usua]==NULL)
						
						{ 
						 echo ' <option value="" selected disabled style="display:none;">Seleccione el Status del Usuario</option>';
       					}
       					else 
       					{
      					
	       					
       						echo' <option selected value="'.$con[stat_usua].'" >'.$con[stat_usua].'</option> ';
       					}
       				
                      		echo "<option value=Activo>Activo</option>";
                      		echo "<option value=Bloqueado>Bloqueado</option>";
	                  
						echo '</select>
					</td>
				</tr>';

				echo '<tr><td>&nbsp;</td></tr>
								<tr>
					<td align="center">	   
						<input type="password" class="validate[';
						if ($boton!='Modificar'){  echo 'required,'; }
						echo ' minSize[6],maxSize[32]] text-input, cajas_entrada" value="" id="pass_usua" name="pass_usua" placeholder="Contraseña" />
					</td>
				</tr>';
			}
			else
			{
				
				echo '<input type="hidden" name="codg_usua" id="codg_usua" value="'.$con['codg_usua'].'">';	
				echo '
					<tr>
						<td align="left">
							<label id="etiqueta" > Cédula de Identidad: </label> <label id="etiqueta"></label> <label id="resultado">'.$con[cedu_usua].' </label> 
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr> 
         		<tr>
         			<td align="left">
         				<label id="etiqueta"> Nombres: </label> <label id="resultado">'.$con[nomb_usua].' </label>
         			</td>
         		</tr>
         		<tr><td>&nbsp;</td></tr>
         		<tr>
         			<td align="left" > <label id="etiqueta"> Apellidos: </label> <label id="resultado">'.$con[apel_usua].' </label> </td> 
         		</tr>
         	 		<tr><td>&nbsp;</td></tr>
         		<tr>
         			<td align="left">
         				<label id="etiqueta"> Correo :</label> <label id="resultado"> '.$con[logi_usua].' </label>
         			</td>
         		</tr>
         		 <tr><td>&nbsp;</td></tr>
         		<tr>
         			<td align="left">
         				<label id="etiqueta"> Tipo de Usuario :</label> <label id="resultado"> '.$nomb_tusu.' </label>
         			</td>
         		</tr>
         		<tr><td>&nbsp;</td></tr>
         		<tr>
         			<td align="left">
         				<label id="etiqueta"> Empresa Relacionada :</label> <label id="resultado"> '.$nomb_empr.' </label>
         			</td>
         		</tr>
         		<tr><td>&nbsp;</td></tr>
         		<tr>
         			<td align="left">
         				<label id="etiqueta"> Status :</label> <label id="resultado"> '.$con[stat_usua].' </label>
         			</td>
         		</tr>
         		<tr><td>&nbsp;</td></tr> ';
			}
			echo '<tr><td>';
			include('sistema/general/botonera.php');
			echo '</td></tr></table>';         		  	   
		?>
	</form>
	<?php } ?>
	<?php // definimos los parametros de busqueda 
		$buscar_parm[0][0]="Cédula";
		$buscar_parm[0][1]="cedu_usua";
		$buscar_parm[1][0]="Nombre";
		$buscar_parm[1][1]="nomb_usua";
		$buscar_parm[2][0]="Apellido";
		$buscar_parm[2][1]="apel_usua";
		$buscar_parm[3][0]="Correo";
		$buscar_parm[3][1]="logi_usua";

		include('sistema/general/busqueda.php');?>
