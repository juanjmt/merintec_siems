<?php 
include("sistema/comunes/verificar_empresa.php");
	// verificar con que empresa esta relacionado el usuario
	$buscando0 = buscar('usuarios','logi_usua',$_SESSION['usuario_logueado'],'individual');
	$con0=$buscando0[0];
	$codg_empr = $con0['codg_empr'];
$boton=$_POST['boton'];
$nacionalidad=$_POST['nacionalidad'];
$cedula=$_POST['cedula'];
$nombre=$_POST['nombre'];
$apellido=$_POST['apellido'];
$sexo=$_POST['sexo'];
$telefono=$_POST['telefono'];
$correo=$_POST['correo'];
$fechanac=$_POST['fechanac'];
$codg_part=$_POST['codg_part'];
$parametro=$_POST['parametro'];
$codg_tpar=$_POST['codg_tpar'];

$con['cedu_part']=$_POST[cedula];
$con['naci_part']=$_POST[nacionalidad];
$con['nomb_part']=$_POST[nombre];
$con['apel_part']=$_POST[apellido];
$con['sexo_part']=$_POST[sexo];
$con['tlfn_part']=$_POST[telefono];
$con['corr_part']=$_POST[correo];
$con['fchn_part']=$_POST[fechanac];
$con['codg_part']=$_POST[codg_part];
$con['codg_tpar']=$_POST[codg_tpar];
/// valores para usar auditoría en caso de eliminar y modificar
$auditoria=$_POST['campo_auditoria'];
/// Preparando datos para guardar
$tabla = "participantes";
$key_entabla = 'codg_part';
$key_enpantalla = $codg_part;
$datos[0] = prepara_datos ("naci_part",$_POST['nacionalidad'],'');
$datos[1] = prepara_datos ("cedu_part",$_POST['cedula'],'');
$datos[2] = prepara_datos ("nomb_part",$_POST['nombre'],'');
$datos[3] = prepara_datos ("apel_part",$_POST['apellido'],'');
$datos[4] = prepara_datos ("sexo_part",$_POST['sexo'],'');
$datos[5] = prepara_datos ("tlfn_part",$_POST['telefono'],'');
$datos[6] = prepara_datos ("corr_part",$_POST['correo'],'');
$datos[7] = prepara_datos ("fchn_part",$_POST['fechanac'],'fecha');
$datos[8] = prepara_datos ("codg_tpar",3,'');
$datos[9] = prepara_datos ("codg_empr",$codg_empr,'');

$tabla2 = "usuarios";
$clave = texto_aleatorio(6);
$datos2[0] = prepara_datos ("cedu_usua",$_POST['cedula'],'');
$datos2[1] = prepara_datos ("nomb_usua",$_POST['nombre'],'');
$datos2[2] = prepara_datos ("apel_usua",$_POST['apellido'],'');
$datos2[3] = prepara_datos ("corr_usua",$_POST['correo'],'');
$datos2[4] = prepara_datos ("logi_usua",$_POST['correo'],'');
$datos2[5] = prepara_datos ("tipo_usua",'2','');
$datos2[6] = prepara_datos ("stat_usua",'Activo','');
if ($boton=='Guardar')
{
	$datos2[7] = prepara_datos ("pass_usua",md5($clave),'');
}

if ($boton=='Guardar'){
	$buscando = buscar($tabla,'cedu_part',$_POST[cedula],'individual');
	$buscando_cor = buscar($tabla,'corr_part',$_POST[correo],'individual');
	$buscando_cor2 = buscar($tabla2,'corr_usua',$_POST[correo],'individual');
	if($buscando[1]<1 && $buscando_cor[1]<1 && $buscando_cor2[1]<1){
		$ejec_guardar = guardar($datos,$tabla);
		if ($ejec_guardar[0]!=''){
			$ejec_guardar_usu = guardar($datos2,$tabla2);
			if($ejec_guardar_usu[0]!=""){
				mail($datos2[3][1], "Datos de Ingreso SIEMS", "Estimado usuario: ".$datos2[1][1]." ".$datos2[2][1]." \n Le informamos que sus datos para ingreso a sistema de SIEMS son: \r\n Usuario: ".$datos2[4][1]."\r\n Contraseña: ".$clave." \r\n Le recomendamos cambiar su clave al ingresar al Sistema.", 'From: merintec@merintec.com.ve' . "\r\n" .
		'Reply-To: merintec@merintec.com.ve' . "\r\n");
			}
			$existente='si';
			$$key_entabla = $ejec_guardar[0];
			$con2 = buscar($tabla,$key_entabla,$ejec_guardar[0],'individual');
			$con=$con2[0];
			$auditoria=$con2[3];
		}
		$mensaje_mostrar=$ejec_guardar[1];
	}else{
		if($buscando_cor[1]==1 || $buscando_cor2[1]==1){
			$mensaje_mostrar = 'Error: El email '.$_POST[corr_part].' ya existe, intente nuevamente';
		}else{
			$mensaje_mostrar = 'Error: La cédula '.$_POST[cedu_part].' ya existe, intente nuevamente';
		}		
		$boton = '';			
	}
}
if ($boton=='Eliminar')
{
	$ejec_eliminar = eliminar($tabla,$key_entabla,$key_enpantalla,$auditoria);
	$mensaje_mostrar=$ejec_eliminar;
	$boton='';
	$auditoria='';
}
if ($boton=='Actualizar')
{
	$buscando = buscar($tabla,'cedu_part',$_POST[cedula]."' AND codg_part<>'".$_POST['codg_part'],'individual');
	$buscando_cor = buscar($tabla,'corr_part',$_POST[correo]."' AND codg_part<>'".$_POST['codg_part'],'individual');
	$buscando_cedpart = buscar($tabla,'codg_part',$_POST[codg_part],'individual');
	$cedpart=$buscando_cedpart[0]['cedu_part'];
	$buscando_cor2 = buscar($tabla2,'corr_usua',$_POST[correo]." AND cedu_usua<>".$cedpart,'individual');
	if($_POST[cedula]!=$cedpart){
		$buscando_ced = buscar($tabla2,'cedu_usua',$_POST[cedula],'individual');
	}
	if($buscando[1]<1 && $buscando_cor[1]<1 && $buscando_cor2[1]<1 && $buscando_ced[1]<1){
		$ejec_actualizar1 = actualizar($datos2,$tabla2,'cedu_usua',$cedpart,$auditoria);
		$ejec_actualizar = actualizar($datos,$tabla,$key_entabla,$key_enpantalla,$auditoria);
		$existente='si';        
		$mensaje_mostrar=$ejec_actualizar[1];
		$$key_entabla = $ejec_actualizar[0];
		$con2 = buscar($tabla,$key_entabla,$ejec_actualizar[0],'individual');
		$con=$con2[0];
		$auditoria=$con2[3];
	}else{
		if($buscando_cor[1]==1 || $buscando_cor2[1]==1){
			$mensaje_mostrar = 'Error: El email '.$_POST[corr_part].' ya existe, intente nuevamente';
		}else{
			$mensaje_mostrar = 'Error: La cédula '.$_POST[cedu_part].' ya existe, intente nuevamente';
		}		
		$boton = 'Actualizando';			
	}
}
if ($boton=='Buscar')
{
	if (strtolower($parametro)!='todos' && strtolower($parametro)!='ultimo'){
		$buscando = buscar($tabla,'codg_empr='.$codg_empr.' AND '.$_POST['criterio'],$parametro,'general');
		$con=$buscando[0];
		$nresultados=$buscando[1];
		$mensaje_mostrar=$buscando[2];
		$auditoria=$buscando[3];
		$$key_entabla = $con[$key_entabla];
		if ($$key_entabla!=NULL) 
		{
			$existente='si';
      	  	}
		else 
		{
			$existente='no';
			$boton='';
		}
	}else{
		$buscando = buscar($tabla,'codg_empr',$codg_empr,'general');
		$con=$buscando[0];
		$nresultados=$buscando[1];
		$mensaje_mostrar=$buscando[2];
		$auditoria=$buscando[3];
		$$key_entabla = $con[$key_entabla];
		if ($$key_entabla!=NULL) 
		{
			$existente='si';
      	  	}
		else 
		{
			$existente='no';
			$boton='';
		}
	}
}
if ($boton=='Nuevo')
{
	$con = array();
	$existente='no';
	$boton='';
	$auditoria='';
}
if ($boton=='Modificar')
{
	$con = buscar($tabla,$key_entabla,$key_enpantalla,'individual');
	$con=$con[0];
	$mensaje_mostrar = "Cambie la información que requiera y presione Actualizar";
	$existente='no';
}
if ($boton=='Actualizando')
{
	$boton = 'Modificar';
}

//consulta combo
$consulta_tpar = mysql_query("SELECT * FROM participantes_tipos order by nomb_tpar ");

if ($con[codg_tpar]!='')
{
	       $codg_tpar=$con[codg_tpar];
       	 $consulta_tpar1 = mysql_query("SELECT * FROM participantes_tipos where codg_tpar='$codg_tpar' ");
       	 $cont1=mysql_fetch_assoc($consulta_tpar1);
       	 $nomb_tpar=$cont1[nomb_tpar];

}

?>

<meta charset="utf-8" />
	<div class="titulo_formulario" align="center">REGISTRO DE PARTICIPANTE</div>
	<?php include('sistema/general/mensaje.php'); ?>
	<?php if ($nresultados>1){ 
		// definimos los parametros a mostrar en el resultado múltiple
		$buscar_varios[0][0]="Cédula";
		$buscar_varios[0][1]="cedu_part";
		$buscar_varios[1][0]="Nombres";
		$buscar_varios[1][1]="nomb_part";
		$buscar_varios[2][0]="Apellidos";
		$buscar_varios[2][1]="apel_part";
		$buscar_varios[3][0]="Fecha Nacimiento";
		$buscar_varios[3][1]="fchn_part";
		$buscar_varios[3][2]="fecha";
		$buscar_varios[4][0]="Tipo";
		$buscar_varios[4][1]="codg_tpar";
		$buscar_varios[4][2]=array("participantes_tipos","codg_tpar","nomb_tpar");
		include('sistema/general/busqueda_varios.php'); 
		echo '<br>'; 
	} 
	else {?>
	<form id="form1" onsubmit="return jQuery(this).validationEngine('validate');"  method="post" action="">
		<table cellpaddig="0" cellspacing="0" border="0" align="center">
      </br>
      </br>	
      <?php 
         /// No Borrar campo usado para auditoría    
	      echo "<input type='hidden' name='campo_auditoria' value='".$auditoria."'>";
         ////////////////////////////////////////
			if ($existente!='si')
         {
				echo '<input type="hidden" name="codg_part" id="codg_part" value="'.$con['codg_part'].'">';	
      		echo '
				<tr>
					<td align="center">	   
						<select name="nacionalidad" id="nacionalidad"  class="validate[required], combo_identificador">
							<option value="'.$con[naci_part].'">'.$con[naci_part].'</option>
							<option value="V">V</option>
							<option value="E">E</option>
						</select>
						<input type="text" class="validate[required, custom[onlyLetterNumber], minSize[6],maxSize[13]] text-input, cajas_entrada2" value="'.$con[cedu_part].'" id="cedula" name="cedula" placeholder="Cédula de Identidad" />
					</td>
				</tr>
				<tr>
          		<td  align="center">
						<input type="text" class="validate[required, custom[onlyLetterSp], minSize[3],maxSize[30]] text-input, cajas_entrada" value="'.$con[nomb_part].'" id="nombre" name="nombre" placeholder="Nombres" />
         		</td>
				</tr>
				<tr>
					<td align="center">
						<input type="text" class="validate[required, custom[onlyLetterSp] , minSize[3],maxSize[30]] text-input,  cajas_entrada" value="'.$con[apel_part].'" id="apellido" name="apellido" placeholder="Apellidos" />
					</td>
				</tr>
				<tr>
					<td>
						<label id="etiqueta">Sexo:</label> ';
        					echo '<input class="validate[required] radio" type="radio" name="sexo" id="masculino"';  if ($con[sexo_part]=='m') { echo 'checked="checked"'; } echo 'value="m"/> <label id="etiqueta">Masculino</label>';
        					echo '<input class="validate[required] radio" type="radio" name="sexo" id="femenino"';  if ($con[sexo_part]=='f') { echo 'checked="checked"'; } echo 'value="f"/> <label id="etiqueta">Femenino</label>';
                		echo '
					</td>
				</tr>
				<tr>
					<td>

						<input  class="validate[required,custom[date]] text-input datepicker cajas_entrada es" type="text" value="'.ordernar_fecha($con[fchn_part]).'" name="fechanac" id="fechanac" placeholder="Fecha de Nacimiento"/>

					</td>
				</tr>
				<tr>
					<td  align="center">
						<input type="text" class="validate[required, custom[phone], minSize[3],maxSize[30]] text-input, cajas_entrada" value="'.$con[tlfn_part].'" id="telefono" name="telefono" placeholder="Teléfono" />
					</td>
				</tr>
				<tr>
					<td align="center">
						<input type="text" class="validate[required, custom[email] , minSize[3],maxSize[100]] text-input,  cajas_entrada" value="'.$con[corr_part].'" id="correo" name="correo" placeholder="Correo Electrónico" />
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>';
			}
			else
			{
				if ($con[sexo_part]=='m')
    			{
    				$sexo='Masculino';
				}
				if ($con[sexo_part]=='f')
    			{
    				$sexo='Femenino';
				}
				echo '<input type="hidden" name="codg_part" id="codg_part" value="'.$con['codg_part'].'">';	
				echo '
					<tr>
						<td align="left">
							<label id="etiqueta" > Cédula de Identidad: </label> <label id="etiqueta"></label> <label id="resultado">'.$con[naci_part].'-'.$con[cedu_part].' </label> 
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr> 
         		<tr>
         			<td align="left">
         				<label id="etiqueta"> Nombres: </label> <label id="resultado">'.$con[nomb_part].' </label>
         			</td>
         		</tr>
         		<tr><td>&nbsp;</td></tr>
         		<tr>
         			<td align="left" > <label id="etiqueta"> Apellidos: </label> <label id="resultado">'.$con[apel_part].' </label> </td> 
         		</tr>
         		<tr><td>&nbsp;</td></tr>
         		<tr>
         			<td align="left">
         				<label id="etiqueta"> Sexo:</label> <label id="resultado"> '.$sexo.' </label>
         			</td>
         		</tr>
         		<tr><td>&nbsp;</td></tr>
         		<tr>
         			<td align="left">
         				<label id="etiqueta"> Fecha de Nacimiento: </label> <label id="resultado"> '.ordernar_fecha($con[fchn_part]).' </label>
         			</td>
         		</tr>
         		<tr><td>&nbsp;</td> </tr>
         		<tr>
         			<td align="left">
         				<label id="etiqueta"> Teléfono:</label> <label id="resultado"> '.$con[tlfn_part].' </label>
         			</td>
         		</tr>
         		<tr><td>&nbsp;</td></tr>
         		<tr>
         			<td align="left">
         				<label id="etiqueta"> Correo :</label> <label id="resultado"> '.$con[corr_part].' </label>
         			</td>
         		</tr>
         		<tr><td>&nbsp;</td></tr> ';
			}
			echo '<tr><td>';
			include('sistema/general/botonera.php');
			echo '</td></tr></table>';         		  	   
		?>
	</form>
	<?php } ?>
	<?php // definimos los parametros de busqueda 
		$buscar_parm[0][0]="Cédula";
		$buscar_parm[0][1]="cedu_part";
		$buscar_parm[1][0]="Nombre";
		$buscar_parm[1][1]="nomb_part";
		include('sistema/general/busqueda.php');
?>
