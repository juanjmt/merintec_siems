<?php 
include("sistema/comunes/verificar_admin_diseno.php");
$boton=$_POST['boton'];
$codg_tipo=$_POST['codg_tipo'];
$nomb_tipo=$_POST['nomb_tipo'];
$parametro=$_POST['parametro'];
/// valores para usar auditoría en caso de eliminar y modificar
$auditoria=$_POST['campo_auditoria'];
/// Preparando datos para guardar
$tabla = "eventos_tipos";
$key_entabla = 'codg_tipo';
$key_enpantalla = $codg_tipo;
$datos[0] = prepara_datos ("nomb_tipo",$_POST['nomb_tipo'],'');
if ($boton=='Guardar'){
	$buscando = buscar($tabla,'nomb_tipo',$_POST[nomb_tipo],'individual');
	if ($buscando[1]<1) {
		$ejec_guardar = guardar($datos,$tabla);
		if ($ejec_guardar[0]!=''){
			$existente='si';
			$$key_entabla = $ejec_guardar[0];
			$con2 = buscar($tabla,$key_entabla,$ejec_guardar[0],'individual');
			$con=$con2[0];
			$auditoria=$con2[3];
			$mensaje_mostrar=$ejec_guardar[1];
		}
	}
	else {
		$mensaje_mostrar = 'Error: El tipo '.$_POST[nomb_tipo].' ya existe intente nuevamente';
		$boton = '';
	}
}
if ($boton=='Eliminar')
{
	$buscando_tipo = buscar('eventos','codg_tipo',$_POST['codg_tipo'],'individual');
	if ($buscando_tipo[1]<1) {
		$ejec_eliminar = eliminar($tabla,$key_entabla,$key_enpantalla,$auditoria);
		$mensaje_mostrar=$ejec_eliminar;
		$boton='';
		$auditoria='';
	}else{
		$mensaje_mostrar='Tipo no puede eliminarse debido a que hay eventos asociados';
		$boton='Eliminando';
	}
}
if ($boton=='Actualizar')
{
	$buscando = buscar($tabla,'nomb_tipo',$_POST[nomb_tipo],'individual');
	if ($buscando[1]<1) {
			$ejec_actualizar = actualizar($datos,$tabla,$key_entabla,$key_enpantalla,$auditoria);
			$existente='si';        
			$mensaje_mostrar=$ejec_actualizar[1];
			$$key_entabla = $ejec_actualizar[0];
			$con2 = buscar($tabla,$key_entabla,$ejec_actualizar[0],'individual');
			$con=$con2[0];
			$auditoria=$con2[3];
	}
	else {
		$mensaje_mostrar = 'Error: El tipo '.$_POST[nomb_tipo].' ya existe intente nuevamente';
		$iramodificar="si";
		$boton = 'Modificar';		
	}
}
if ($boton=='Buscar')
{
	$buscando = buscar($tabla,$_POST['criterio'],$parametro,'individual');
	$con=$buscando[0];
	$nresultados=$buscando[1];
	$mensaje_mostrar=$buscando[2];
	$auditoria=$buscando[3];
	$$key_entabla = $con[$key_entabla];
	if ($$key_entabla!=NULL) 
	{
		$existente='si';
        }
	else 
	{
		$existente='no';
		$boton='';
	}
}
if ($boton=='Nuevo')
{
	$existente='no';
	$boton='';
   $auditoria='';
}
if ($boton=='Modificar')
{
	$con = buscar($tabla,$key_entabla,$key_enpantalla,'individual');
	$con=$con[0];
	$mensaje_mostrar = "Cambie la información que requiera y presione Actualizar";
	if($iramodificar){ $mensaje_mostrar .= "<br><br>No ha efectuado cambios o ya existe el tipo"; }
	$existente='no';
}
if ($boton=='Eliminando')
{
	$existente='si';
	$con = buscar($tabla,$key_entabla,$key_enpantalla,'individual');
	$con=$con[0];
	$boton='Buscar';
}
?>
<meta charset="utf-8" />
	<div class="titulo_formulario" align="center">REGISTRO DE TIPOS DE EVENTOS</div>
	<?php include('sistema/general/mensaje.php'); ?>
	<?php if ($nresultados>1){ 
		// definimos los parametros a mostrar en el resultado múltiple
		$buscar_varios[0][0]="Tipo de evento";
		$buscar_varios[0][1]="nomb_tipo";
		include('sistema/general/busqueda_varios.php'); 
		echo '<br>'; 
	} 
	else {?>
	<form id="form1" onsubmit="return jQuery(this).validationEngine('validate');"  method="post" action="">
		<table cellpaddig="0" cellspacing="0" border="0" align="center">
      </br>
      </br>	
		<?php 
         /// No Borrar campo usado para auditoría    
	      echo "<input type='hidden' name='campo_auditoria' value='".$auditoria."'>";
         ////////////////////////////////////////
			if ($existente!='si')
			{
				echo '<input type="hidden" name="codg_tipo" id="codg_tipo" value="'.$con[codg_tipo].'">';	
				echo '
				<tr>
					<td  align="center">
						<input type="text" class="validate[required, custom[onlyLetterSp], minSize[3],maxSize[30]] text-input, cajas_entrada" value="'.$con[nomb_tipo].'" id="nomb_tipo" name="nomb_tipo" placeholder="Tipo de Evento" />
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>'; 
			}
			else
			{
				echo '<input type="hidden" name="codg_tipo" id="codg_tipo" value="'.$con[codg_tipo].'">';	
				echo '
				<tr>
					<td align="left" colspan="3">
						<label id="etiqueta">Tipo de Evento: </label> <label id="resultado">'.$con[nomb_tipo].' </label>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>';
			}
			echo '<tr><td>';
			include('sistema/general/botonera.php');
			echo '</td></tr></table>';         		  	   
		?>
	</form>
	<?php } ?>
	<?php // definimos los parametros de busqueda 
		$buscar_parm[0][0]="Tipo de Evento";
		$buscar_parm[0][1]="nomb_tipo";
		include('sistema/general/busqueda.php');?>
