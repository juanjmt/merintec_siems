﻿<?php   
error_reporting(0);
include('../comunes/conexion.php');
include('../comunes/funciones_php.php');
include("../comunes/verificar_admin_vendedor_gestion.php");
$key_entabla = 'codg_intr'; 
$key_entabla1 = 'tipo_intr'; 
$key_entabla2 = 'nomb_intr'; 
$key_entabla3 = 'nomb_even'; 
$campos_pasa = $key_entabla.'|-|'.$key_entabla1.'|-|'.$key_entabla2.'|-|'.$key_entabla3;
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <title>.:: SIEMS Instituto Gerencial ::.</title>
		<style type="text/css" title="currentStyle">
			@import "../datatables/media/css/demo_page.css";
			@import "../datatables/media/css/demo_table.css";
		</style>
		<script type="text/javascript" language="javascript" src="../datatables/media/js/jquery.js"></script>
		<script type="text/javascript" language="javascript" src="../datatables/media/js/jquery.dataTables.js"></script>
		<link rel="stylesheet" href="../comunes/calendario/jquery-ui.css">
	        <script src="../comunes/calendario/jquery-ui.min.js"></script>


		<script type="text/javascript" charset="utf-8">
			function eliminar_contacto(id){
				var parametros = {
			    	"id" : id
			   	};
				$.ajax({
					data:  parametros,
				    url:   'eliminar_contacto.php',
				    type:  'post',
				    success:  function (response) 
				    {
				    	alert('El interesado se ha eliminado exitosamente');
				    	form2.submit();
				    }
				});
				return;
			} 
			function pasar_valor(valores, campos)
			{
				var recibe_valor = valores;
				var val_elem = recibe_valor.split('|-|');
					codigo = val_elem[0];
					tipoin = val_elem[1];
					nombre = val_elem[2];
					evento = val_elem[3];
				var recibe_campos = campos;
				var cam_elem = recibe_campos.split('|-|');
					codigo_input = cam_elem[0];
					tipoin_input = cam_elem[1];
					nombre_input = cam_elem[2];
					evento_input = cam_elem[3];
					
				if (document.form1[codigo_input].value==codigo){
					document.form1[codigo_input].value = '';
					document.form1[tipoin_input].value = '';
					document.form1[nombre_input].value = '';
					document.form1[evento_input].value = '';
				}
				else {
					document.form1[codigo_input].value = codigo;
					document.form1[tipoin_input].value = tipoin;
					document.form1[nombre_input].value = nombre;
					document.form1[evento_input].value = evento;
					document.form1['archivar'].checked = false;
				}
				if (document.form1[codigo_input].value==''){
					document.all['contacto_adicionales'].style.display = "none";
				}
				else{
					document.all['contacto_adicionales'].style.display = "block";
				}
			}
		</script>
		<!-- <script src="../validacion/js/jquery-1.8.2.min.js" type="text/javascript"></script> -->
	<script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
	<script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
	<link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
        <link rel="stylesheet" href="../comunes/calendario/jquery-ui.css">
	<link href="../css/sm_estilos.css" rel="stylesheet" type="text/css">
        <script src="../comunes/calendario/jquery-ui.min.js"></script> 
        <!-- validacion en vivo -->
<script type="text/javascript" charset="utf-8">
			var oTable;			
			$(document).ready(function() {
				/*<!-- Acción sobre el botón con id=boton y actualizamos el div con id=capa -->
				$("#boton").click(function(event) {
                    $("#capa").load("gestion_contacto_history.php",{valor1:'primer valor', valor2:'segundo valor'}, function(response, status, xhr) {
                          if (status == "error") {
                            var msg = "Error!, algo ha sucedido: ";
                            $("#capa").html(msg + xhr.status + " " + xhr.statusText);
                          }
                        });
                });*/
				
			    // binds form submission and fields to the validation engine
                jQuery("#form1").validationEngine('attach', {bindMethod:"live"});
                
                $("#boton_correos").click(function() {
                	var tabla_objeto = $('#example tbody tr');
					var texto = '';
					for (var i = 1; i <= tabla_objeto.length; i++) {
						var actual = $('#example tr:nth-child(' + i + ') td:nth-child(5)').text();
						if (texto.search(actual) < 0 ){ texto = texto + actual + ','; }
					}
					texto = texto + ',';
					texto = texto.replace(",,","");
					$('#correos').val(texto);
					$('#correos').select();
				});

                $("#boton_telef").click(function() {
                	var tabla_objeto = $('#example tbody tr');
					var texto = '';
					for (var i = 1; i <= tabla_objeto.length; i++) {
						var actual = $('#example tr:nth-child(' + i + ') td:nth-child(6)').text();
						if (texto.search(actual) < 0 ){ texto = texto + actual + ','; }
					}
					texto = texto + ',';
					texto = texto.replace(",,","");
					$('#telefonos').val(texto);
					$('#telefonos').select();
				});

                $("#boton_clear").click(function() {
					$('#correos').val('');
					$('#telefonos').val('');
				});

				$("#example tbody tr").click( function( e ) {
					if ( $(this).hasClass('row_selected') ) {
						$(this).removeClass('row_selected');						
						pasar_valor('','<?php echo $campos_pasa; ?>');
					}
					else {
						oTable.$('tr.row_selected').removeClass('row_selected');
						$(this).addClass('row_selected');
						var anSelected = fnGetSelected( oTable );
						var codigo = anSelected[0].id;
						var recibe_codigo = codigo;
						var val_codg = recibe_codigo.split('|-|');
						var codigo1 = val_codg[0];
						var codigo2 = val_codg[1];
						pasar_valor(codigo,'<?php echo $campos_pasa; ?>');
						$("#capa").load("gestion_contacto_history.php",{valor1:codigo1, valor2:codigo2}, function(response, status, xhr) {
                          if (status == "error") {
                            var msg = "Cargando información...";
                            $("#capa").html(msg + xhr.status + " " + xhr.statusText);
                          }
                        });
					}
				});	
				/* Add a click handler for the delete row */
				$('#delete').click( function() {
					var anSelected = fnGetSelected( oTable );
					if ( anSelected.length !== 0 ) {
						oTable.fnDeleteRow( anSelected[0] );
					}
				} );
				
				oTable = $('#example').dataTable( {
				"sPaginationType": "full_numbers",
					"oLanguage": {
						"sLengthMenu": "Mostrar _MENU_ registros por página",
						"sZeroRecords": "Nada encontrado - Intenta nuevamente",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Showing 0 to 0 of 0 records",
			                        "sSearch": "Buscar:",
						"sInfoFiltered": "(filtados de _MAX_ registros)",
						"oPaginate": {
				                        "sFirst": "Primera",
				                        "sPrevious": "Anterior",
				                        "sNext": "Siguiente",
				                        "sLast": "Última"
				                 }
					}
				} );
			} );
			
			/* Get the rows which are currently selected */
			function fnGetSelected( oTableLocal )
			{
				return oTableLocal.$('tr.row_selected');
			}
		</script>
        <!-- CALENDARIO-->
  <script>
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			$( ".datepicker" ).datepicker({
				firstDay: 1,
				closeText: 'Cerrar',
				nextText: 'Sig ->',
				currentText: 'Hoy',
				prevText: '<- Ant',
				monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
				monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
				dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
				dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié;', 'Juv', 'Vie', 'Sáb'],
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
				dateFormat: 'dd/mm/yy',			
				changeYear: true
			});
			jQuery("form1").validationEngine();
		});	
	</script>
    <link href="../css/sm_estilos.css" rel="stylesheet" type="text/css">
    <link href="../../css/estilos.css" rel="stylesheet" type="text/css">
    </head>
  <body id="dt_example" class="ex_highlight_row">
<?php
if ($_POST['boton']=='Guardar'){
	$sql_guardar = "INSERT INTO gestion_contacto (codg_rela,orgn_rela,obsr_gest,dest_gest,fcha_gest,codg_usua,stat_gest) VALUES (".$_POST[$key_entabla].",'".$_POST[$key_entabla1]."','".$_POST['obsr_gest']."','".$_POST['destino']."','".date('Y-m-d')."','".$_POST['codg_usua']."','".$_POST['archivar']."')";
	mysql_query($sql_guardar);
};
?>
	<table border="0" cellpadding="1" cellspacing="1" width="100%">
		<tr>
			<td align="center" id="contacto_cabecera">
				Módulo de Gestión de Contacto
			</td>
		</tr>	
		<tr>
			<td align="center" id="contacto_opciones">
				<form name="form2" id="form1" method="POST" action="" >
					<?php $consulta_eventos = mysql_query("SELECT * FROM eventos order by nomb_evnt "); 
						echo '<select name="codg_evnt" id="codg_evnt"  class="validate[required], combo_form" onchange="submit()">';
						if ($_POST[codg_evnt]==NULL)
						{ 
						 	echo ' <option value="" selected disabled style="display:none;">Seleccione el Evento</option>';
       						}
       						while($fila=mysql_fetch_array($consulta_eventos))
                  				{
							if ($_POST[codg_evnt]==$fila[codg_evnt]){ $add_sel='selected'; }else {$add_sel='';}
                      					echo "<option value=".$fila[codg_evnt]." ".$add_sel.">".$fila[nomb_evnt]."</option>";
                  				}
						echo '</select>';
						if (!$_POST[fini_rep]){ 
							$fecha = date('Y-m-j');
							$nuevafecha = strtotime ( '-6 month' , strtotime ( $fecha ) ) ;
							$fecha_inicio = date ( 'Y-m-j' , $nuevafecha ); 
						} else {$fecha_inicio = $_POST[fini_rep]; } 
						if (!$_POST[ffin_rep]){ $fecha_final = date('d-m-Y'); } else {$fecha_final = $_POST[ffin_rep]; }
					?>
					Del <input  class="validate[required,custom[date]] text-input datepicker cajas_entrada es" type="text" name="fini_rep" id="fini_rep" value="<?php echo $fecha_inicio; ?>" placeholder="Fecha de Inicio" style="width: 100px; text-align:center;" onchange="submit()">
					Al <input  class="validate[required,custom[date]] text-input datepicker cajas_entrada es" type="text" name="ffin_rep" id="ffin_rep" value="<?php echo $fecha_final; ?>" placeholder="Fecha de Fin" style="width: 100px; text-align:center;" onchange="submit()">
					<input type="checkbox" name="mostrar_archivados" value="SI" <?php if ($_POST['mostrar_archivados']){ echo 'checked'; }?> onclick="submit();"> Mostrar Archivados
				</form>
			</td>
		</tr>
	</table>
	<div id="container">
	<div id="demo">
	<form name="form1" id="form1" method="POST" action="" >
				<div align="center" id="contacto_adicionales" style="display: none;">
					<div style="cursor: pointer; right: 50px;; position: absolute;"><img width="30px" title="Eliminar Permanentemente" src='../imagenes/icono_rechazado.png' onclick="eliminar_contacto(codg_intr.value);"><br>Eliminar</div>
				   	<table width="500px" height="300px" border="0" cellpadding="0" cellspacing="0" align="center">
						<tr height="20px">
							<td colspan="4">
								<span class="contacto_etiquetas">Tipo de Contacto:</span> <input class="contacto_resultado" style="border: 0px #FFFFFF; width: 250px; background: transparent;" readonly="yes" type="text" value="" name="<?php echo $key_entabla1; ?>" id="<?php echo $key_entabla1; ?>">
				            </td>
						</tr>
				        <tr height="20px">
				        	<td colspan="4">
								<span class="contacto_etiquetas">Nombre:</span> <input class="contacto_resultado" style="border: 0px #FFFFFF; width: 250px; background: transparent;" readonly="yes"  type="text" value="" name="<?php echo $key_entabla2; ?>" id="<?php echo $key_entabla2; ?>">
				            </td>
					</tr>
				        <tr height="20px">
				        	<td colspan="4">										
								<span class="contacto_etiquetas">Evento:</span> <textarea class="contacto_resultado" style="border: 0px #FFFFFF; resize: none;  background: transparent;" cols="60" rows="2" name="<?php echo $key_entabla3; ?>" id="<?php echo $key_entabla3; ?>" readonly="readonly"></textarea>
							</td>
					</tr>
				        <tr height="20px">
				        	 <td align='center'><input class="validate[required] radio"type="radio" name="destino" value="telefono"><img src='../imagenes/gestion/telefono_on.png' title='Contactado vía telefónica' border='0'></td>
				             <td align='center'><input class="validate[required] radio"type="radio" name="destino" value="mail"><img src='../imagenes/gestion/mail_on.png' title='Contactado vía e-mail' border='0'></td>
				             <td align='center'><input class="validate[required] radio"type="radio" name="destino" value="sms"><img src='../imagenes/gestion/sms_on.png' title='Contactado vía SMS' border='0'></td>
				             <td align='center'><input class="validate[required] radio"type="radio" name="destino" value="skype"><img src='../imagenes/gestion/skype_on.png' title='Contactado vía Skype' border='0'></td>
					</tr>
				        <tr align="center">
				        	<td colspan="4">							
				            	<textarea cols="60" rows="5" maxlength="255" name="obsr_gest" id="obsr_gest" class="validate[required, minSize[5],maxSize[255]] text-input, cajas_texto" placeholder="Observación a registrar"></textarea><input type="hidden" name="codg_usua" value="<?php echo $_SESSION['codg_usuario']; ?>"></td>
				        </tr>
					<tr height="20px">
				        	<td colspan="4">										
								<span class="contacto_etiquetas"><input type="checkbox" name="archivar" id="archivar" value="SI">Archivar este contacto (No Mostrar Nuevamente)</span><br><br> 
							</td>
					</tr>
				   	<tr height="20px" align="center">
				   		<td colspan="4">
        				   		<input type="hidden" value="" name="<?php echo $key_entabla; ?>" id="<?php echo $key_entabla; ?>">
								<input type="submit" name="boton" id="boton" value="Guardar" class="boton_iniciar">
								<input type="button" name="boton1" id="boton1" value="Cerrar" class="boton_iniciar" OnClick="document.all['contacto_adicionales'].style.display = 'none';">			   								
				   		</td>
				   	</tr>
				   </table>
				   	<div id="contacto_history_titulo">HISTORIAL DE CONTACTO</div>
					<div id="contacto_history_cabecera">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr align="center">
								<td width="70px">FECHA</td><td width="50px">MEDIO</td><td>OBSERVACIÓN</td><td td width="150px">USUARIO</td>
							</tr>
						</table>
					</div>
				   	<div id="capa" class="contacto_history">Historial sobre el contacto</div>
				</div>
			</form>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
	<thead>
		<tr>
			<?php
				$nparam_v = 8;	
                                $cabeceras = '<th width="180px">Fecha</th><th width="120px">Evento</th><th width="150px">Nombre de Contacto</th><th width="50px">Tipo</th><th width="5px">e-mail</th><th width="50px">Teléfono</th><th width="5px">TLF</th><th width="5px">mail</th><th width="5px">SMS</th><th width="5px">Skype</th><th width="5px">Archivo</th><th>Última Acción</th>';
				echo $cabeceras;
		   ?>
		</tr>
	</thead>
	<tbody>
		<?php 
		$fecha_ini = fecha_formato($fecha_inicio, 2);
		$fecha_fin = fecha_formato($fecha_final, 2);
            $sql_contactos = "SELECT *, (if (codg_insc IS NOT NULL, 'Inscritos', 'Interesados')) as tipo_intr FROM interesados inte, eventos ev, eventos_apertura ev_ap WHERE inte.codg_aper = ev_ap.codg_aper AND ev_ap.codg_evnt=ev.codg_evnt AND fech_intr >= '".$fecha_ini."' AND fech_intr <= '".$fecha_fin."'";
	    if ($_POST[codg_evnt]){
		$sql_contactos .= " AND ev.codg_evnt=".$_POST[codg_evnt];
	    }
	    if ($_POST['mostrar_archivados']) {
		// Si se desea que solo se muestren los archivados debe activar esta línea sino se mostrarán los archivados junto al resto    
		//$sql_contactos .= " AND 'SI' = (SELECT stat_gest FROM gestion_contacto WHERE codg_rela=inte.codg_intr ORDER BY codg_gest DESC LIMIT 1) ";
	    }
	    else {
		$sql_contactos .= " AND ('' = (SELECT stat_gest FROM gestion_contacto WHERE codg_rela=inte.codg_intr ORDER BY codg_gest DESC LIMIT 1) OR codg_intr NOT IN (SELECT codg_rela FROM gestion_contacto) ) ";
	    }
            $sql_contactos .= " UNION SELECT *,'' as codg_aper,'' as fini_aper,'' as ffin_aper,'' as obsr_aper,'' as codg_evnt,'' as pgen_aper,'' as prec_aper,'' as prep_aper,'' as prem_aper, '' as pmin_aper,'' as hras_aper, '' as stat_aper,'' as imag_aper1,'' as imag_aper2, '' as ciudad_aper, '' as  desc_aper, '' as estu_desc, '' as prof_desc, '' as empr_desc, '' as dcon_aper, '' as dura_aper, 'Interesados Antiguos' as tipo_intr FROM interesados inte, eventos ev WHERE inte.codg_curso = ev.codg_evnt AND fech_intr >= '".$fecha_ini."' AND fech_intr <= '".$fecha_fin."'";
	    if ($_POST[codg_evnt]){
		$sql_contactos .= " AND inte.codg_curso=".$_POST[codg_evnt];
	    }
	    if ($_POST['mostrar_archivados']) {
		// Si se desea que solo se muestren los archivados debe activar esta línea sino se mostrarán los archivados junto al resto    
		//$sql_contactos .= " AND 'SI' = (SELECT stat_gest FROM gestion_contacto WHERE codg_rela=inte.codg_intr ORDER BY codg_gest DESC LIMIT 1) ";
	    }
	    else {
		$sql_contactos .= " AND ('' = (SELECT stat_gest FROM gestion_contacto WHERE codg_rela=inte.codg_intr ORDER BY codg_gest DESC LIMIT 1) OR codg_intr NOT IN (SELECT codg_rela FROM gestion_contacto) ) ";
	    }
	    $sql_contactos .= " UNION SELECT inte_cat.*,'' as codg_aper,'' as fini_aper,'' as ffin_aper,'' as obsr_aper,'' as codg_evnt,'' as pgen_aper,'' as prec_aper,'' as prep_aper,'' as prem_aper, '' as pmin_aper,'' as hras_aper, '' as stat_aper,'' as imag_aper1,'' as imag_aper2, '' as ciudad_aper, '' as  desc_aper, '' as estu_desc, '' as prof_desc, '' as empr_desc, '' as dcon_aper, '' as dura_aper, '' as codg_evnt,'Catálogo PDF' as nomb_evnt,'' as desc_evnt,'' as codg_area,'' as codg_tipo,'' as stat_evnt,'' as imag_evnt,'Sólicito Catálogo' as tipo_intr FROM interesados inte_cat WHERE codg_aper = -1 AND fech_intr >= '".$fecha_ini."' AND fech_intr <= '".$fecha_fin."'";
	    if (!$_POST['mostrar_archivados']) {
		$sql_contactos .= " AND ('' = (SELECT stat_gest FROM gestion_contacto WHERE codg_rela=inte_cat.codg_intr ORDER BY codg_gest DESC LIMIT 1) OR codg_intr NOT IN (SELECT codg_rela FROM gestion_contacto) ) ";
	    }
//	    echo $sql_contactos;
            $bus_contactos =  mysql_query($sql_contactos);
			while($res = mysql_fetch_array($bus_contactos)){
			    $n_mail = 'off';
			    $n_tlfn = 'off';
			    $n_sms = 'off';
			    $n_skype = 'off';
			    $n_stat = 'off';
				$obsr_gest = '&nbsp';
			        $sql_gest = "SELECT g.*, 'off' as n_mail, if ((select count(codg_gest) from gestion_contacto where codg_rela=g.codg_rela and dest_gest='mail')>0,'on','off') as n_mail, if ((select count(codg_gest) from gestion_contacto where codg_rela=g.codg_rela and dest_gest='telefono')>0,'on','off') as n_tlfn, if((select count(codg_gest) from gestion_contacto where codg_rela=g.codg_rela and dest_gest='skype')>0,'on','off') as n_skype, if((select count(codg_gest) from gestion_contacto where codg_rela=g.codg_rela and dest_gest='sms')>0,'on','off') as n_sms, if((SELECT stat_gest FROM gestion_contacto WHERE codg_rela=g.codg_rela ORDER BY codg_gest DESC LIMIT 1)='SI','on','off') as stat_gest FROM gestion_contacto g WHERE g.codg_rela = ".$res['codg_intr']." AND g.orgn_rela='".$res['tipo_intr']."' ORDER BY codg_gest DESC LIMIT 1";
//echo '<BR>';				
$bus_gest = mysql_query($sql_gest);
				if ($res_gest = mysql_fetch_array($bus_gest)){ 
					$n_mail = $res_gest['n_mail'];
					$n_tlfn = $res_gest['n_tlfn'];
					$n_sms = $res_gest['n_sms'];
					$n_skype = $res_gest['n_skype'];
					$n_stat = $res_gest['stat_gest'];
					$obsr_gest = $res_gest['obsr_gest'];
				}
				
				echo "<tr class'gradeX' id='".$res['codg_intr']."|-|".$res['tipo_intr']."|-|".$res['nomb_intr']."|-|".$res['nomb_evnt']."'>";
						echo "<td>";
                			        echo fecha_formato($res['fech_intr'], 2);
				                echo "</td>";
						echo "<td>";
                			        echo $res['nomb_evnt'];
				                echo "</td>";
                			        echo "<td>";
                			        echo $res['nomb_intr'];
				                echo "</td>";
                			        echo "<td>";
                			        echo $res['tipo_intr'];
				                echo "</td>";
   				                echo "<td align='center'>".$res['corr_intr']."</td>";
   				                echo "<td align='center'>".$res['tlfn_intr']."</td>";
				                echo "<td align='center'><img src='../imagenes/gestion/telefono_".$n_tlfn.".png' title='Contactado vía telefónica ".$res['tlfn_intr']."' border='0'></td>";
				                echo "<td align='center'><img src='../imagenes/gestion/mail_".$n_mail.".png' title='Contactado vía e-mail ".$res['corr_intr']."' border='0'></td>";
				                echo "<td align='center'><img src='../imagenes/gestion/sms_".$n_sms.".png' title='Contactado vía SMS ".$res['tlfn_intr']."' border='0'></td>";
				                echo "<td align='center'><img src='../imagenes/gestion/skype_".$n_skype.".png' title='Contactado vía Skype ' border='0'></td>";
                			        echo "<td align='center'><img src='../imagenes/gestion/stat_".$n_stat.".png' title='Archivado' alt='Archivado' border='0'></td>";
                			        echo "<td>";
                			        echo $obsr_gest;
				                echo "</td>";
				echo "</tr>";
		   } 
		?>
	</tbody>
	<tfoot>
		<tr>
			<?php   
				echo $cabeceras;
		   ?>
		</tr>
	</tfoot>
</table>
<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td><input class="buscar_curso" style="" type="button" id="boton_correos" name="boton_correos" value="Obtener Correos"><td>
		<td><textarea id="correos" name="correos" rows="4" cols="35" readonly="readonly"></textarea><br><b>Nota:</b> Se obviarán los e-mails repetidos</td>
		<td><input class="buscar_curso" type="button" id="boton_telef" name="boton_telef" value="Obtener Teléfonos"><td>
		<td><textarea id="telefonos" name="telefonos" rows="4" cols="35" readonly="readonly"></textarea><br><b>Nota:</b> Se obviarán los teléfonos repetidos</td>
		<td><input class="buscar_curso" type="button" id="boton_clear" name="boton_clear" value="Limpiar" title="Vaciar ambas cajas de texto"><td>
	</tr>
</table>
			</div>
		</div>
  </body>
</html>
