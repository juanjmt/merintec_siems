<?php 
include("sistema/comunes/verificar_admin_administrador.php");
$boton=$_POST['boton'];
$nomb_banc=$_POST['nomb_banc'];
$numr_cuen=$_POST['numr_cuen'];
$tipo_cuen=$_POST['tipo_cuen'];
$trans_cuen=$_POST['trans_cuen'];
$titu_cuen=$_POST['titu_cuen'];
$rift_cuen=$_POST['rift_cuen'];
$cort_cuen=$_POST['cort_cuen'];
$codg_banc=$_POST['codg_banc'];
$parametro=$_POST['parametro'];
/// valores para usar auditoría en caso de eliminar y modificar
$auditoria=$_POST['campo_auditoria'];
/// Preparando datos para guardar
$tabla = "banco";
$key_entabla = 'codg_banc';
$key_enpantalla = $codg_banc;
$datos[0] = prepara_datos ("nomb_banc",$_POST['nomb_banc'],'');
$datos[1] = prepara_datos ("numr_cuen",$_POST['numr_cuen'],'');
$datos[2] = prepara_datos ("tipo_cuen",$_POST['tipo_cuen'],'');
$datos[3] = prepara_datos ("trans_cuen",$_POST['trans_cuen'],'');
$datos[4] = prepara_datos ("titu_cuen",$_POST['titu_cuen'],'');
$datos[5] = prepara_datos ("rift_cuen",$_POST['rift_cuen'],'');
$datos[6] = prepara_datos ("cort_cuen",$_POST['cort_cuen'],'');

if ($boton=='Guardar'){
	$buscando = buscar($tabla,'numr_cuen',$_POST[numr_cuen],'individual');
	if ($buscando[1]<1) {
		$ejec_guardar = guardar($datos,$tabla);
		if ($ejec_guardar[0]!=''){
			$existente='si';
			$$key_entabla = $ejec_guardar[0];
			$con2 = buscar($tabla,$key_entabla,$ejec_guardar[0],'individual');
			$con=$con2[0];
			$auditoria=$con2[3];
			$mensaje_mostrar=$ejec_guardar[1];
		}
	}else{
		$mensaje_mostrar = 'Error: El Número '.$_POST[numr_cuen].' ya existe intente nuevamente';
		$boton = '';
	}	
}

if ($boton=='Eliminar')
{
	$buscando_tipo = buscar('pagos','codg_banc',$_POST['codg_banc'],'individual');
	if ($buscando_tipo[1]<1) {
		$ejec_eliminar = eliminar($tabla,$key_entabla,$key_enpantalla,$auditoria);
		$mensaje_mostrar=$ejec_eliminar;
		$boton='';
		$auditoria='';
	}else{
		$mensaje_mostrar='Banco no puede eliminarse debido a que hay pagos asociados';
		$boton='Eliminando';
	}
}
if ($boton=='Actualizar')
{
	$buscando = buscar($tabla,'numr_cuen',$_POST[numr_cuen],'individual');
	if ($buscando[1]<1) {
			$ejec_actualizar = actualizar($datos,$tabla,$key_entabla,$key_enpantalla,$auditoria);
			$existente='si';        
			$mensaje_mostrar=$ejec_actualizar[1];
			$$key_entabla = $ejec_actualizar[0];
			$con2 = buscar($tabla,$key_entabla,$ejec_actualizar[0],'individual');
			$con=$con2[0];
			$auditoria=$con2[3];
	}
	else {
		$mensaje_mostrar = 'Error: El Número '.$_POST[numr_cuen].' ya existe intente nuevamente';
		$iramodificar="si";
		$boton = 'Modificar';		
	}	
	
	
}
if ($boton=='Buscar')
{
	$buscando = buscar($tabla,$_POST['criterio'],$parametro,'general');
	$con=$buscando[0];
	$nresultados=$buscando[1];
	$mensaje_mostrar=$buscando[2];
	$auditoria=$buscando[3];
	$$key_entabla = $con[$key_entabla];
	if ($$key_entabla!=NULL) 
	{
		$existente='si';
        }
	else 
	{
		$existente='no';
		$boton='';
	}
}
if ($boton=='Nuevo')
{
	$existente='no';
	$boton='';
   $auditoria='';
}
if ($boton=='Modificar')
{
	$con = buscar($tabla,$key_entabla,$key_enpantalla,'individual');
	$con=$con[0];
	$mensaje_mostrar = "Cambie la información que requiera y presione Actualizar";
	if($iramodificar){ $mensaje_mostrar .= "<br><br>No ha efectuado cambios o ya existe el Número"; }
	$existente='no';
}
if ($boton=='Eliminando')
{
	$existente='si';
	$con = buscar($tabla,$key_entabla,$key_enpantalla,'individual');
	$con=$con[0];
	$boton='Buscar';
}
?>
<meta charset="utf-8" />
	<div class="titulo_formulario" align="center">REGISTRO DE BANCOS</div>
	<?php include('sistema/general/mensaje.php'); ?>
	<?php if ($nresultados>1){ 
		// definimos los parametros a mostrar en el resultado múltiple
		$buscar_varios[0][0]="Nombre";
		$buscar_varios[0][1]="nomb_banc";
		$buscar_varios[1][0]="Número de Cuenta";
		$buscar_varios[1][1]="numr_cuen";
		$buscar_varios[2][0]="Tipo de Cuenta";
		$buscar_varios[2][1]="tipo_cuen";
		$buscar_varios[3][0]="Titulo de la Cuenta";
		$buscar_varios[3][1]="titu_cuen";
		$buscar_varios[4][0]="Utilidad";
		$buscar_varios[4][1]="trans_cuen";
		//$buscar_varios[4][2]="center";
		include('sistema/general/busqueda_varios.php'); 
		echo '<br>'; 
	} 
	else {?>
	<form id="form1" onsubmit="return jQuery(this).validationEngine('validate');"  method="post" action="">
		<table cellpaddig="0" cellspacing="0" border="0" align="center">
      </br>
      </br>	
      <?php 
         /// No Borrar campo usado para auditoría    
	      echo "<input type='hidden' name='campo_auditoria' value='".$auditoria."'>";
         ////////////////////////////////////////
		if ($existente!='si')
       	{
         	if ($con[tipo_cuen]=='A')
         	{
         	   $tipo_cuenta='Ahorro';	
         	   
         	}
         	if ($con[tipo_cuen]=='C')
         	{
         	   $tipo_cuenta='Corriente';
         		
         	}

		if ($con[trans_cuen]==1)
         	{
         	   $trans_cuenta='Transferencia';	
         	   
         	}
         	if ($con[trans_cuen]==0)
         	{
                   $trans_cuenta='Movimiento';
         		
         	}
         	
         	
         	echo '<input type="hidden" name="codg_banc" id="codg_banc" value="'.$con['codg_banc'].'">';	
				echo '
				<tr>
					<td  align="center">
						<input type="text" class="validate[required, custom[onlyLetterNumber], minSize[3],maxSize[30]] text-input, cajas_entrada" value="'.$con[nomb_banc].'" id="nomb_banc" name="nomb_banc" placeholder="Banco" />
					</td>
				</tr>
				<tr>
					<td align="center">
						<input type="text" class="validate[required, custom[integer] , minSize[20],maxSize[20]] text-input,  cajas_entrada" value="'.$con[numr_cuen].'" id="numr_cuen" name="numr_cuen" placeholder="Número de Cta" />
					</td>
				</tr>
				<tr>
					<td align="center">	   
						<select name="tipo_cuen" id="tipo_cuen"  class="validate[required], combo_form" >';
						if ($con[tipo_cuen]==NULL)
						{ 
						 echo ' <option value="" selected disabled style="display:none;">Seleccione Tipo de Cta</option>';
       				 }
       				 else
       				 {       				 
						 echo' <option    value="'.$con[tipo_cuen].'" >'.$tipo_cuenta.'</option> ';
						 }
						 echo'	<option value="A">Ahorro</option>
							<option value="C">Corriente</option>
						</select>
					
					</td>
				</tr>
				
<tr>
					<td align="center">	   
						<select name="trans_cuen" id="trans_cuen"  class="validate[required], combo_form" >';
						if ($con[trans_cuen]==NULL)
						{ 
						 echo ' <option value="" selected disabled style="display:none;">Seleccione la Utilidad</option>';
       				 }
       				 else
       				 {       				 
						 echo' <option    value="'.$con[trans_cuen].'" >'.$trans_cuenta.'</option> ';
						 }
						 echo'	<option value="1">Transferencia</option>
							<option value="0">Movimientos</option>
						</select>
					
					</td>
				</tr>


<tr>
					<td  align="center">
						<input type="text" class="validate[required, custom[onlyLetterNumber], minSize[3],maxSize[60]] text-input, cajas_entrada" value="'.$con[titu_cuen].'" id="titu_cuen" name="titu_cuen" placeholder="Titular de la Cuenta" />
					</td>
				</tr>
				<tr>
					<td  align="center">
						<input type="text" class="validate[required, custom[onlyLetterNumber], minSize[3],maxSize[10]] text-input, cajas_entrada" value="'.$con[rift_cuen].'" id="rift_cuen" name="rift_cuen" placeholder="Rif o cédula  del titular" />
					</td>
				</tr>
				<tr>
					<td  align="center">
						<input type="text" class="validate[required, custom[email], minSize[3],maxSize[60]] text-input, cajas_entrada" value="'.$con[cort_cuen].'" id="cort_cuen" name="cort_cuen" placeholder="Correo del Titular" />
					</td>
				</tr>	'; 
			}
			else 
			{
				if ($con[tipo_cuen]=='A')
			 	{
			 	   $tipo_cuenta='Ahorro';	
			 	}
			 	if ($con[tipo_cuen]=='C')
			 	{
			 		$tipo_cuenta='Corriente';
			 	}
				if ($con[trans_cuen]==1)
			 	{
			 	   $trans_cuenta='Transferencia';	
			 	}
			 	if ($con[trans_cuen]==0)
			 	{
			 	   $trans_cuenta='Movimientos';
			 	}
				echo '<input type="hidden" name="codg_banc" id="codg_banc" value="'.$con['codg_banc'].'">';	
				echo '
					<tr>
						<td align="left">
							<label id="etiqueta" > Nombre: </label> <label id="etiqueta"></label> <label id="resultado">'.$con[nomb_banc].' </label> 
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<td align="left">
							<label id="etiqueta"> Número de Cta: </label> <label id="resultado">'.$con[numr_cuen].' </label>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr> 
					<tr>
						<td align="left"> 
							<label id="etiqueta"> Tipo de Cta: </label> <label id="resultado">'.$tipo_cuenta.' </label> 
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr> 
<tr>
						<td align="left"> 
							<label id="etiqueta"> Utilidad de Cta: </label> <label id="resultado">'.$trans_cuenta.' </label> 
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr> 
					<tr>
						<td align="left">
							<label id="etiqueta">Titular de la Cuenta:</label> <label id="resultado"> '.$con[titu_cuen].' </label>
						</td>
					<tr/>
					<tr><td>&nbsp;</td></tr> 
					<tr>
						<td align="left">
							<label id="etiqueta">Rif del Titular:</label> <label id="resultado"> '.$con[rift_cuen].' </label>
						</td>
					<tr/>
					<tr><td>&nbsp;</td></tr> 
					<tr>
						<td align="left">
							<label id="etiqueta">Correo del Titular:</label> <label id="resultado"> '.$con[cort_cuen].' </label>
						</td>
					<tr/>
					
					<tr><td>&nbsp;</td></tr>';
			}
			echo '<tr><td>';
			include('sistema/general/botonera.php');
			echo '</td></tr></table>';
                   ?>
   	   </form>
	<?php } ?>
	<?php // definimos los parametros de busqueda 
		$buscar_parm[0][0]="Nombre";
		$buscar_parm[0][1]="nomb_banc";
		$buscar_parm[1][0]="Titular";
		$buscar_parm[1][1]="titu_cuen";
		$buscar_parm[2][0]="Rif";
		$buscar_parm[2][1]="rift_cuen";
		$buscar_parm[3][0]="Correo";
		$buscar_parm[3][1]="cort_cuen";
		include('sistema/general/busqueda.php');?>
